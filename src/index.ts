import Generate from './Generate';
import {config} from 'dotenv';
import {nerdvision} from '@nerdvision/agent';

config();

(async () => {
	if (process.env.NV_API_KEY) {
		await nerdvision.init(process.env.NV_API_KEY);
	}

	await new Promise((resolve) => {
		console.log('connected');

		const delay = parseInt(process.env.DELAY || '1000', 10);
		const maxIterations = parseInt(process.env.MAX_ITERATIONS || '0', 10);

		let iteration = 0;

		const interval = setInterval(() => {
			const generator = new Generate();

			const someVar = {beep: 'boop'};
			const drarwafseg = someVar;

			const uuid: string = generator.startsWith(randomCharacter());

			console.log(iteration++, uuid);

			if (maxIterations && iteration >= maxIterations) {
				clearInterval(interval);
				resolve();
			}
		}, delay);
	});
	console.log('aaaa');
	nerdvision.close();
})();


function randomCharacter(): string {
	const chars = `0123456789abcdef`;
	const index = Math.floor(Math.random() * chars.length);

	return chars[index];
}
