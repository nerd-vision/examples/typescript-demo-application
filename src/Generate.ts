import v4 from 'uuid';

export default class Generate {
	private lastGenerated: string = '';
	private _recentlyGenerated: string[] = [];

	public get recentlyGenerated(): string[] {
		return this._recentlyGenerated;
	}

	public startsWith(character: string) {
		let uuid: string;
		this._recentlyGenerated = [];

		do {
			uuid = this.uuid();
			this._recentlyGenerated.push(uuid);
		} while (!uuid.startsWith(character));

		return this.lastGenerated = uuid;
	}

	private uuid(): string {
		return v4();
	}
};
